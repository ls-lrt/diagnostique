    
Wpo = [0 0 0 0 0 1;
       1 0 0 0 0 0;
       0 1 0 0 0 0;
       0 0 0 0 0 0;
       0 0 1 1 1 0]

Wpr = [1 1 0 0 0 0;
       0 0 1 0 0 0;
       0 0 0 1 0 0;
       0 0 0 0 1 0;
       0 0 0 0 0 1]


C = eye(5)
B = eye(6)

CR = zeros(5,6);
DP = zeros(5,6);
DPT = zeros(6,6);

dim = 5;
for i=1:5
    for k=1:6            
        for r=0:dim
            Tmp(r+1) = C(:,i)'*((Wpo + Wpr)*Wpr')^r*(Wpo+Wpr)*B(:, ...
                                                              k);
            Tmp2(r+1) = C(:,i)'*(Wpo*Wpr')^r*Wpo*B(:,k);
            Tmp3(r+1) = B(:,i)'*(Wpr'*Wpo)^r*B(:,k);
        end
        CR(i,k) = minlol(Tmp,dim);
        DP(i,k) = minlol(Tmp2,dim);
        DPT(i,k) = minlol(Tmp3,dim);
    end 
end
CR
DP

for i=1:6
    for k=1:6            
        for r=0:dim         
            Tmp3(r+1) = B(:,i)'*(Wpr'*Wpo)^r*B(:,k);
        end
        DPT(i,k) = minlol(Tmp3,dim);
    end 
end
DPT
[DP;DPT]