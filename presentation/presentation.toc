\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Introduction: diagnosis of DES with Petri Net}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Diagnosis of DES with Petri Net}{4}{0}{1}
\beamer@sectionintoc {2}{Petri nets for DES}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Background notions}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Example}{7}{0}{2}
\beamer@subsectionintoc {2}{3}{Notions for sensibility analysis}{8}{0}{2}
\beamer@sectionintoc {3}{Structural sensitivity }{11}{0}{3}
\beamer@subsectionintoc {3}{1}{Causality Relationship}{13}{0}{3}
\beamer@subsectionintoc {3}{2}{Direct Path}{21}{0}{3}
\beamer@sectionintoc {4}{Diagnosabilty of PN using structral sensitivity}{30}{0}{4}
\beamer@subsectionintoc {4}{1}{Influence and dependence area}{31}{0}{4}
\beamer@subsectionintoc {4}{2}{Diagnosability based on $DP$ and $CR$}{34}{0}{4}
\beamer@subsubsectionintoc {4}{2}{1}{Theory}{34}{0}{4}
\beamer@subsubsectionintoc {4}{2}{2}{Application to the example}{35}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{37}{0}{5}
