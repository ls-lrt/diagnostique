    
Wpo = [0 0 0 0 0 0 1;
       1 0 0 0 0 0 0;
       0 1 0 0 0 0 0;
       0 0 0 0 1 0 0;
       0 0 1 1 0 1 0]

Wpr = [1 1 0 0 0 0 0;
       0 0 1 0 0 0 0;
       0 0 0 1 1 0 0;
       0 0 0 0 0 1 0;
       0 0 0 0 0 0 1]

C = eye(5)
B = eye(7)

CR = zeros(5,7);
DP = zeros(5,7);

dim = 5;
for i=1:5
    for k=1:7             
        for r=0:dim
            Tmp(r+1) = C(:,i)'*((Wpo + Wpr)*Wpr')^r*(Wpo+Wpr)*B(:, ...
                                                              k);
            Tmp2(r+1) = C(:,i)'*(Wpo*Wpr')^r*Wpo*B(:,k);
        end
        CR(i,k) = minlol(Tmp,dim);
        DP(i,k) = minlol(Tmp2,dim);
    end 
end
CR
DP
