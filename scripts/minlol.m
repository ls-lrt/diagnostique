function x=minlol(a,r)
    min = 0;
    for k = 1:r
        if (a(k) > 0) 
            min = k;
            break;
        end
    end
    x = min - 1;
end
